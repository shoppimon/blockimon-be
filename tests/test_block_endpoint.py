"""Block endpoint test cases
"""
import json
import pytest
from flask import Flask
from blockimon import create_app, db


@pytest.fixture
def app():
    app = Flask(__name__)
    app.testing = True
    create_app(app, extra_config={
        'SQLALCHEMY_DATABASE_URI': 'sqlite:////tmp/blockimon-test-db.sqlite',
        'SQLALCHEMY_POOL_SIZE': None
    })

    return app


def setup_function():
    with app().app_context():
        db.drop_all()
        db.create_all()


def _create_single_block(client):
    data = {
        "category_id": 1234,
        "name": "Add Product to Cart",
        "type": "step",
        "content": {},
        "parameters": {
            "var_key": {
                "name": "My Variable",
                "description": "An important variable that is useful for something",
                "default": None,
                "validation": []
            },
        }
    }

    return client.post('/block',
                       data=json.dumps(data),
                       content_type='application/json')


def test_get_all_returns_empty(client):
    """Test that we get an empty list when calling get with no argument and no data
    """
    resp = client.get('/block')
    assert resp.status_code == 200
    assert resp.json == []


def test_create_block(client):
    """Test creating a block then fetching it
    """
    resp = _create_single_block(client)
    assert resp.status_code == 201
    assert resp.json['id'] is not None

    resp = client.get('/block')
    blocks = resp.json
    assert len(blocks) == 1
    assert blocks[0]['type'] == 'step'


def test_create_two_blocks(client):
    _create_single_block(client)
    _create_single_block(client)

    resp = client.get('/block')
    blocks = resp.json
    assert len(blocks) == 2
    assert blocks[0]['id'] != blocks[1]['id']


def test_get_single_block(client):
    resp = _create_single_block(client)
    block_id = resp.json['id']

    resp = client.get('/block/{id}'.format(id=block_id))
    block = resp.json
    assert block['id'] == block_id


def test_update_block(client):
    resp = _create_single_block(client)
    block = resp.json
    block['type'] = 'validation'
    block['name'] = "Check for Something"

    resp = client.put('/block/{id}'.format(id=block['id']),
                       data=json.dumps(block),
                       content_type='application/json')

    assert resp.status_code == 200
    assert resp.json['id'] == block['id']
    assert resp.json['type'] == 'validation'
    assert resp.json['name'] == block['name']

    resp = client.get('/block/{id}'.format(id=block['id']))
    assert resp.json['type'] == 'validation'
    assert resp.json['name'] == block['name']


def test_patch_block(client):
    resp = _create_single_block(client)
    block_id = resp.json['id']
    original = client.get('/block/{id}'.format(id=block_id))
    assert original.json['type'] == 'step'

    resp = client.patch('/block/{id}'.format(id=block_id),
                        data=json.dumps({"type": "validation"}),
                        content_type='application/json')
    assert resp.status_code == 200

    modified = client.get('/block/{id}'.format(id=block_id))
    assert modified.json['type'] == 'validation'


def test_delete_block(client):
    resp = _create_single_block(client)
    block_id = resp.json['id']

    resp = client.get('/block')
    assert len(resp.json) == 1

    resp = client.delete('/block/{id}'.format(id=block_id))
    assert resp.status_code == 204

    resp = client.get('/block')
    assert len(resp.json) == 0
