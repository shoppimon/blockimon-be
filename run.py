#!/usr/bin/env python

"""Development purposes executable for the Blockimon server
"""

from blockimon.application import app

if __name__ == '__main__':
    app.run()
