from flask import Flask
from flask_cors import CORS
# from flask_apispec import FlaskApiSpec
# from apispec import APISpec

from blockimon import default_config, error_handling
from blockimon.model import db, Block
from blockimon import resource


def create_app(app=None, extra_config=None, extra_config_file=None):
    if app is None:
        app = Flask(__name__)

    app.config.from_object(default_config)

    if extra_config_file:
        app.config.from_pyfile(extra_config_file)

    if extra_config:
        app.config.update(**extra_config)

    CORS(app)
    db.init_app(app)
    with app.app_context():
        db.create_all()

    resource.Block.register_routes(app, '/block')

    # app.config.update({
    #     'APISPEC_SPEC': APISpec(
    #         title='Blockimon API',
    #         version='1.0.0',
    #         plugins=(
    #             'apispec.ext.flask',
    #             'apispec.ext.marshmallow'
    #         ),
    #     ),
    #     'APISPEC_SWAGGER_URL': '/swagger/',
    # })
    #
    # docs = FlaskApiSpec(app)
    # docs.register(resource.Block, endpoint='block')

    error_handling.init_app(app)

    return app
