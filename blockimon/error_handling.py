"""Error handling for Shoppimon REST API Flask endpoints
"""
from flask import jsonify
from werkzeug.exceptions import default_exceptions


def app_api_problem_json(ex):
    """Generic application/problem+json error handler
    """
    code = ex.code if hasattr(ex, 'code') else 500
    data = {"type": "http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
            "title": ex.name if hasattr(ex, 'name') else "Internal Server Error",
            "detail": ex.description if hasattr(ex, 'description') else str(ex),
            "status": code}

    response = jsonify(data)
    response.headers['Content-Type'] = 'application/problem+json'
    response.status_code = code

    return response


def init_app(app):
    for code in default_exceptions:
        app.errorhandler(code)(app_api_problem_json)
