"""Entity schemas
"""

from marshmallow import Schema, fields, validate


class Block(Schema):
    """Block entity schema
    """

    id = fields.Int(dump_only=True)
    category_id = fields.Int(required=True)
    name = fields.Str(required=True)
    type = fields.Str(validate=[validate.OneOf(['step', 'validation'])], required=True)
    content = fields.List(fields.Dict(), required=True)  # TODO: nested?
    parameters = fields.Dict(required=False)  # TODO: nested?

    class Meta:
        strict = True

    # TODO: contextual validation that parameters and content match