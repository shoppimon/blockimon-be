from blockimon import create_app
import os

_config_file = os.environ.get('CONFIG_FILE', None)
app = create_app(extra_config_file=_config_file)
