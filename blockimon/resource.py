"""REST resources
"""
from flask import make_response
from flask_apispec import use_kwargs, marshal_with, Ref
from flask_apispec.views import MethodResource
from werkzeug.exceptions import NotFound

from blockimon.model import Block as BlockModel, db
from blockimon.schema import Block as BlockSchema


@marshal_with(Ref('schema'))
class BaseResource(MethodResource):
    """Base class for REST resources
    """
    schema = None

    @classmethod
    def register_routes(cls, app, url, name=None, pk=None, pk_type='int'):
        if name is None:
            name = cls.__name__.lower()
        if pk is None:
            pk = name + '_id'

        view_func = cls.as_view(name)
        app.add_url_rule(url, defaults={pk: None},
                         view_func=view_func, methods=['GET', ])
        app.add_url_rule(url, view_func=view_func, methods=['POST', ])

        rule = '{url}/<{pk_type}:{pk}>'.format(url=url, pk_type=pk_type, pk=pk)
        app.add_url_rule(rule, view_func=view_func,
                         methods=['GET', 'PUT', 'PATCH', 'DELETE'])


class Block(BaseResource):
    """Block resource
    """

    schema = BlockSchema

    def get(self, block_id):
        """Get a block or list blocks
        ---
        get:
            parameters:
                - in: path
                  schema: BlockSchema
            responses:
                200:
                    schema: BlockSchema
        """
        if block_id is None:
            return self._list()
        else:
            return self._get(block_id)

    @marshal_with(BlockSchema(many=True))
    def _list(self):
        """List all blocks

        TODO: query parameter support
        """
        return BlockModel.query.all()

    def _get(self, block_id):
        """Get a specific block
        """
        block = BlockModel.query.get(block_id)
        if block:
            return block
        else:
            raise NotFound("Requested block does not exist")

    @use_kwargs(BlockSchema)
    def post(self, **kwargs):
        block = BlockModel(**kwargs)
        db.session.add(block)
        db.session.commit()
        return block, 201

    @marshal_with(None)
    def delete(self, block_id):
        block = BlockModel.query.get(block_id)
        if block:
            db.session.delete(block)
        else:
            raise NotFound("Requested block does not exist")
        db.session.commit()
        return make_response('', 204)

    @use_kwargs(BlockSchema)
    def put(self, block_id, **kwargs):
        return self._update(block_id, **kwargs)

    @use_kwargs(BlockSchema(partial=True))
    def patch(self, block_id, **kwargs):
        return self._update(block_id, **kwargs)

    @staticmethod
    def _update(block_id, **kwargs):
        block = BlockModel.query.get(block_id)
        if not block:
            raise NotFound("Requested block does not exist")

        block.update_from_dict(**kwargs)
        db.session.commit()
        return block
