"""Blockimon Model Classes
"""

import json

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, String, Text, TypeDecorator
from sqlalchemy.dialects.mysql import INTEGER

db = SQLAlchemy()


class JSONEncodedValue(TypeDecorator):
    """Custom type for json-serialized object stored in a DB VARCHAR column
    """
    impl = Text

    def process_bind_param(self, value, engine):
        if value is None:
            return None
        return json.dumps(value)

    def process_result_value(self, value, engine):
        if value is not None:
            return json.loads(value)
        return value


class DictUpdatableMixin:
    def update_from_dict(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class Block(db.Model, DictUpdatableMixin):
    __tablename__ = 'blocks'

    id = Column(INTEGER(unsigned=True), primary_key=True, autoincrement=True, nullable=False)
    category_id = Column(INTEGER(unsigned=True), nullable=False)
    name = Column(String(40), nullable=False)
    type = Column(String(15), nullable=False)
    content = Column(JSONEncodedValue(), nullable=False)
    parameters = Column(JSONEncodedValue())
