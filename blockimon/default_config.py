"""Default configuration
"""

SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://shoppimon:shoppimon@127.0.0.1/shoppimon?charset=utf8"
SQLALCHEMY_POOL_SIZE = 10
SQLALCHEMY_POOL_RECYCLE = 60 * 60 * 8
SQLALCHEMY_TRACK_MODIFICATIONS = False

CORS_ORIGINS = [r'^https://.*\.shoppimon\.com$']
