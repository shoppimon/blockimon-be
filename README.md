Blockimon Server
================

Blockimon is an internal tool / prototype for building custom scenarios out of
a library of managed building blocks.

This repository includes the server side of Blockimon - i.e. the REST API and 
backend for managing building blocks and other server-side functionality 
required by the tool. 
 
## Setup

    cd blockimon-be
    virtualenv venv --python=python3.6
    . venv/bin/activate
    pip install -r requirements.txt -e .

### Create a configuration file
Create a configuration file in `blockimon/blockimon.cfg`. You can copy the 
provided `blockimon.cfg.dist` and alter its content. Some important
configuration options are:

    # Database URL (SQLAchemy format)
    SQLALCHEMY_DATABASE_URI="mysql+mysqlconnector://username:password@host/shoppimon?charset=utf8"


## Running in development
Run the server with the following commands:

    export FLASK_APP=blockimon
    export CONFIG_FILE=<configuration file path>
    flask run --debugger --reload
