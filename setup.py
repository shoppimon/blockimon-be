from setuptools import setup

setup(
    name='blockimon',
    packages=['blockimon'],
    include_package_data=True,
    install_requires=[
        'Flask',
        'Flask-APISpec',
        'Flask-CORS',
        'Flask-SQLAlchemy',
        'SQLAlchemy',
        'mysql-connector-python-rf'
    ],
)